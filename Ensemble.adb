package body ensemble is

   ------------------------
   -- construireEnsemble --
   ------------------------

   function construireEnsemble return Type_Ensemble is
      unEnsemble: Type_Ensemble;
   begin
      for i in 1..9 loop
         unEnsemble(i):=False;
      end loop;
      return unEnsemble;
   end construireEnsemble;

   ------------------
   -- ensembleVide --
   ------------------

   function ensembleVide (e : in Type_Ensemble) return Boolean is
      i: Integer; -- Indice de parcours de l'ensemble
   begin
      i:=1;
      while i<=9 and then not e(i) loop
         i:=i+1;
      end loop;

      return i>9;

   end ensembleVide;

--     -----------------------
--     -- appartientChiffre --
--     -----------------------

   function appartientChiffre
     (e : in Type_Ensemble;
      v :    Integer)
      return Boolean
   is
   begin
     return e(v);
   end appartientChiffre;

--     --------------------
--     -- nombreChiffres --
--     --------------------
--
   function nombreChiffres (e : in Type_Ensemble) return Integer is
      i: Integer; -- Indice de parcours de l'ensemble
      nbOccurrences: Integer;
   begin
      nbOccurrences:=0;
      i:=1;
      while i<=9 loop
         if e(i) then
            nbOccurrences:=nbOccurrences+1;
         end if;
         i:=i+1;
      end loop;

      return nbOccurrences;

   end nombreChiffres;

--     --------------------
--     -- ajouterChiffre --
--     --------------------

   procedure ajouterChiffre (e : in out Type_Ensemble; v : in Integer) is
   begin
      if e(v) then
         raise APPARTIENT_ENSEMBLE;
      end if;
         e(v):=True;
   end ajouterChiffre;

   --------------------
   -- retirerChiffre --
   --------------------

   procedure retirerChiffre (e : in out Type_Ensemble; v : in Integer) is
   begin
      if not e(v) then
         raise NON_APPARTIENT_ENSEMBLE;
      end if;
      e(v):=False;
   end retirerChiffre;

end ensemble;
