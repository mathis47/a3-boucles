-- bibliothèques d'entrée sortie
with Ada.Text_IO;         use Ada.Text_IO;
with Ada.Integer_Text_IO; use Ada.Integer_Text_IO;
package body resolutions is

   -----------------------
   -- estChiffreValable --
   -----------------------

   function estChiffreValable
     (g : in Type_Grille;
      v :    Integer;
      c :    Type_Coordonnee)
      return Boolean
   is
      coord,coin : Type_Coordonnee;
   begin
      if not caseVide(g,c) then
         raise CASE_NON_VIDE;
      end if;

      for i in 1..9 loop
         coord:=construireCoordonnees(obtenirLigne(c),i);
         if obtenirChiffre(g,coord) = v then
            return false;
         end if;
         coord:=construireCoordonnees(i,obtenirColonne(c));
         if obtenirChiffre(g,coord) = v then
            return false;
         end if;
      end loop;
      coin:=obtenirCoordonneeCarre(obtenirCarre(c));
      for j in 0..2 loop
         for k in 0..2 loop
            coord:=construireCoordonnees(obtenirLigne(coin)+j,obtenirColonne(coin)+k);
            if obtenirChiffre(g,coord) = v then
               return false;
            end if;
         end loop;
      end loop;
      return true;
   end estChiffreValable;

   -------------------------------
   -- obtenirSolutionsPossibles --
   -------------------------------

   function obtenirSolutionsPossibles
     (g : in Type_Grille;
      c : in Type_Coordonnee)
      return Type_Ensemble
   is
      valeursPossibles:Type_Ensemble;
   begin
      valeursPossibles:=construireEnsemble;
      for i in 1..9 loop
         if caseVide(g,c) and then estChiffreValable(g,i,c) then
            ajouterChiffre(valeursPossibles,i);
         end if;
      end loop;
      return valeursPossibles;


   end obtenirSolutionsPossibles;

   ------------------------------------------
   -- rechercherSolutionUniqueDansEnsemble --
   ------------------------------------------

   function rechercherSolutionUniqueDansEnsemble
     (resultats : in Type_Ensemble)
      return Integer
   is
   begin
      if ensembleVide(resultats) then raise ENSEMBLE_VIDE; end if;
      if nombreChiffres(resultats)=1 then
         for i in 1..9 loop
            if appartientChiffre(resultats,i) then return i;
            end if;
         end loop;
      else raise PLUS_DE_UN_CHIFFRE; end if;
      return 0;
   end rechercherSolutionUniqueDansEnsemble;

   function SolutionEstUnique
     (resultats : in Type_Ensemble)
                                            return Boolean is
   begin
      if ensembleVide(resultats) then raise ENSEMBLE_VIDE; end if;
      if nombreChiffres(resultats)=1 then return True; else return False; end if;

   end SolutionEstUnique;

   --------------------
   -- resoudreSudoku --
   --------------------

   procedure resoudreSudoku
     (g      : in out Type_Grille;
      trouve :    out Boolean)
   is
      coord:Type_Coordonnee;
      valeursPossibles:Type_Ensemble;
   begin
      trouve:=False;
      while not trouve loop
         for i in 1..9 loop
            for j in 1..9 loop
               coord:=construireCoordonnees(i,j);
               valeursPossibles:=obtenirSolutionsPossibles(g,coord);
               if caseVide(g,coord) and then SolutionEstUnique(valeursPossibles) then
                  fixerChiffre(g,coord,rechercherSolutionUniqueDansEnsemble(valeursPossibles));
                  afficherGrille (g);
               end if;
            end loop;
         end loop;
         if estRemplie(g) then trouve:=True; end if;
      end loop;
   end resoudreSudoku;

end resolutions;
