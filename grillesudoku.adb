package body grilleSudoku is

   ----------------------
   -- construireGrille --
   ----------------------

   function construireGrille return Type_Grille is
      g:Type_Grille;
      coord:Type_Coordonnee;
   begin
      for i in 1..9 loop
         for j in 1..9 loop
            coord:=construireCoordonnees(i,j);
            fixerChiffre(g,coord,0);
         end loop;
      end loop;
      return g;
   end construireGrille;

   --------------
   -- caseVide --
   --------------

   function caseVide
     (g : in Type_Grille;
      c : in Type_Coordonnee)
      return Boolean
   is
   begin

      return g(obtenirLigne(c),obtenirColonne(c))=0;
   end caseVide;

   --------------------
   -- obtenirChiffre --
   --------------------

   function obtenirChiffre
     (g : in Type_Grille;
      c : in Type_Coordonnee)
      return Integer
   is
   begin
      --if caseVide(g,c) then raise OBTENIR_CHIFFRE_NUL; end if;
      return g(obtenirLigne(c),obtenirColonne(c));
   end obtenirChiffre;

   --------------------
   -- nombreChiffres --
   --------------------

   function nombreChiffres (g : in Type_Grille) return Integer is
      k:Integer;
      coord:Type_Coordonnee;
   begin
      k:=0;
      for i in 1..9 loop
         for j in 1..9 loop
            coord:=construireCoordonnees(i,j);
            if not CaseVide(g,coord) then
               k:=k+1;
            end if;
         end loop;
      end loop;
      return k;
   end nombreChiffres;

   ------------------
   -- fixerChiffre --
   ------------------

   procedure fixerChiffre
     (g : in out Type_Grille;
      c : in     Type_Coordonnee;
      v : in     Integer)
   is
   begin
      --if not caseVide(g,c) then raise FIXER_CHIFFRE_NON_NUL; end if;
      g(obtenirLigne(c),obtenirColonne(c)):=v;
   end fixerChiffre;

   ---------------
   -- viderCase --
   ---------------

   procedure viderCase (g : in out Type_Grille; c : in out Type_Coordonnee) is
   begin
      if caseVide(g,c) then raise VIDER_CASE_VIDE; end if;
      g(obtenirLigne(c),obtenirColonne(c)):=0;
   end viderCase;

   ----------------
   -- estRemplie --
   ----------------

   function estRemplie (g : in Type_Grille) return Boolean is
   begin
      return nombreChiffres(g) = 81;
   end estRemplie;

   ------------------------------
   -- obtenirChiffresDUneLigne --
   ------------------------------

   function obtenirChiffresDUneLigne
     (g        : in Type_Grille;
      numLigne : in Integer)
      return Type_Ensemble
   is
      valeursPossibles:Type_Ensemble;
      coord:Type_Coordonnee;
   begin
      valeursPossibles:=construireEnsemble;
      for i in 1..9 loop
         coord:=construireCoordonnees(numLigne,i);
         if not caseVide(g,coord) then
            ajouterChiffre(valeursPossibles,obtenirChiffre(g,coord));
         --else ajouterChiffre(valeursPossibles,obtenirChiffre(g,coord));
         end if;
      end loop;
      return valeursPossibles;
   end obtenirChiffresDUneLigne;

   --------------------------------
   -- obtenirChiffresDUneColonne --
   --------------------------------

   function obtenirChiffresDUneColonne
     (g          : in Type_Grille;
      numColonne : in Integer)
      return Type_Ensemble
   is
      valeursPossibles:Type_Ensemble;
      coord:Type_Coordonnee;
   begin
      valeursPossibles:=construireEnsemble;
      for i in 1..9 loop
         coord:=construireCoordonnees(i,numColonne);
         if not caseVide(g,coord) then
            ajouterChiffre(valeursPossibles,obtenirChiffre(g,coord));
         --else ajouterChiffre(valeursPossibles,obtenirChiffre(g,coord));
         end if;
      end loop;
      return valeursPossibles;
   end obtenirChiffresDUneColonne;

   -----------------------------
   -- obtenirChiffresDUnCarre --
   -----------------------------

   function obtenirChiffresDUnCarre
     (g        : in Type_Grille;
      numCarre : in Integer)
      return Type_Ensemble
   is
      valeursPossibles:Type_Ensemble;
      coord:Type_Coordonnee;
   begin
      valeursPossibles:=construireEnsemble;
      for i in 0..2 loop
         for j in 0..2 loop
            coord:=construireCoordonnees(obtenirLigne(obtenirCoordonneeCarre(numCarre))+i,obtenirColonne(obtenirCoordonneeCarre(numCarre))+j);
            if not caseVide(g,coord) then
               ajouterChiffre(valeursPossibles,obtenirChiffre(g,coord));
            --else ajouterChiffre(valeursPossibles,obtenirChiffre(g,coord));
            end if;
         end loop;
      end loop;
      return valeursPossibles;
   end obtenirChiffresDUnCarre;

end grilleSudoku;
