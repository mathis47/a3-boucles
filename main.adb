with grilleSudoku;        use grilleSudoku;
with ensemble;            use ensemble;
with Coordonnee;          use Coordonnee;
with affichage;           use affichage;
with resolutions;         use resolutions;
with Ada.Text_IO;         use Ada.Text_IO;
with Ada.Integer_Text_IO; use Ada.Integer_Text_IO;
with remplirGrille;       use remplirGrille;

procedure main is
   g      : Type_Grille;

   trouve : Boolean;


begin
   g:=grille7Difficile;
   resoudreSudoku(g,trouve);
end main;
